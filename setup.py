# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='Parrot',
      version='0.1a',
      description='Figuring out traffic status using Twitter!',
      author=u'Saulo Lordão Andrade Barros',
      author_email='saulo.lordao@gmail.com'
     )
