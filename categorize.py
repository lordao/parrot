from math import ceil, trunc
import os
import glob

from nltk import *
import psycopg2
import psycopg2.extensions

from parrot.processor import make_status_bag
from parrot.database import StatusDao
import pickle

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

CONNECTION_STRING = "dbname=twitter user=saulo"

class Categorizer(object):
    def __init__(self):
        self.classifiers = [pickle.load(open(filename, 'r')) for filename in glob.glob('classifiers/*.pickle')]
        self.categories = get_categories()
        self.file_output = []
        self.tweets_dictionary = get_texts()
        self.tweets = self.tweets_dictionary.items()

    def categorize_all(self):
        i = 1
        n = len(self.tweets)
        for tweet in self.tweets:
            os.system("clear")
            print "%d of %d (%.0f%%):" % (i, n, 100 * (i)/float(n))
            self.categorize_tweet(tweet)
            i += 1

    def categorize_tweet(self, tweet):
        status_id = tweet[0]
        category = self.show_tweet(tweet)
        category = parse_input(category)
        if category is None or category <= 0:
            return None
        update_category(status_id, category)

    def show_tweet(self, tweet):
        status_id = tweet[0]
        username = self.tweets_dictionary[status_id]["username"]
        text = self.tweets_dictionary[status_id]["text"]
        print "  %s: " % username
        print "    %s\n" % text
        for j in range(trunc(ceil(len(self.categories)/5.0))):
            print self.categories[j*5:(j+1)*5]
        return raw_input("\nChoose the category id or type in a new one: ")

    def check_answer(self, status_id, category_id):
        bag = make_status_bag(status_id)[0]
        impact = self.category_to_impact(category_id)
        got_it_right = filter(lambda c: impact == c.classify(bag), self.classifiers)
        got_it_right = map(lambda c: c.__class__.__name__, got_it_right)

        self.file_output.append(' '.join(got_it_right))

    def category_to_impact(self, category_id):
        for (c_id, _, impact) in self.categories:
            if c_id == category_id:
                return impact
        else:
            return None

def main():
    status_id = None
    username = None
    text = None
    file_output = ""
    print "Loading tweets..."
    texts = get_texts()
    bags, _ = bag_of_words()
    print "Done!\nSelecting uncategorized tweets..."
    tweets = filter(lambda bag: bag[1] is None, bags)
    n = len(tweets)
    categories = get_categories()
    print "Done! %d tweets to categorize." % n
    raw_input()
    for i in range(n):
        os.system("clear")
        status_id = tweets[i][0]
        print status_id
        username = tweets[i][1]
        text = texts[status_id]['text']
        processed_text = processed_texts[status_id]['processed_text']
        print "%d of %d (%.0f%%):" % (i+1, n, 100 * (i+1)/float(n))
        print "  %s: " % username
        print "    %s\n" % text
        print "    %s\n" % processed_text
        for j in range(trunc(ceil(len(categories)/5.0))):
            print categories[j*5:(j+1)*5]

        inferred_categories = infer_category()

        category = raw_input("\nChoose the category id or type in a new one: ")
        if category == '':
            continue
        try:
            category_id = int(category)
            if category_id < 0:
                print "Bye bye!"
                break
            elif category_id == 0:
                continue
        except:
            category_id = get_category_id(category)
            categories = get_categories()
        file_output += check_answer(texts[status_id], category_id)
        update_category(status_id, category_id)

def parse_input(category):
    if category == '':
        return None
    try:
        category_id = int(category)
    except:
        category_id = get_category_id(category)
    return category_id


def infer_category(bag):
    return map(lambda c:
               {
                   'name': c.__class__.__name__,
                   'id'  : c.classify(bag)
               }, classifiers)

def get_texts():
    status_dao = StatusDao()
    texts = status_dao.list_status_texts()
    return dict(map(lambda (a,b,c,d): (a,{ 'text': b, 'processed_text': c , 'username': d}), texts))


def get_categories():
    categories = None
    with psycopg2.connect(CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT id, name, impact_on_traffic FROM category ORDER BY id")
            try:
                categories = cur.fetchall()
            except:
                categories = []
    conn.close()
    return categories

def update_category(status_id, category_id):
    with psycopg2.connect(CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            sql = "UPDATE status SET category_id = %(category_id)s WHERE id = %(id)s"
            cur.execute(sql, {"category_id": category_id, "id": status_id})
    conn.close()

def get_category_id(category_name):
    category_id = None
    with psycopg2.connect(CONNECTION_STRING) as conn:
        with conn.cursor() as cur:
            sql = "SELECT id FROM category WHERE name = %(name)s"
            cur.execute(sql, {"name": category_name})
            result = cur.fetchone()
            if result is not None:
                category_id = result[0]
            else:
                sql = "INSERT INTO category (name) VALUES (%(name)s)"
                cur.execute(sql, {"name": category_name})
                sql = "SELECT id FROM category WHERE name = %(name)s"
                cur.execute(sql, {"name": category_name})
                category_id = cur.fetchone()[0]
    conn.close()
    return category_id

if __name__ == "__main__":
    main()
