# -*- coding: UTF-8 -*-
"""Module responsible for processing tweets."""
import time

import nltk
from nltk.corpus import stopwords
from parrot.nltk_casual_8dc4849 import TweetTokenizer
import regex as re
from twitter import TwitterError

import parrot.database as db
import parrot.regex_bank as regex_bank
import parrot.replacers as replacers
import parrot.twitter_exceptions as t_ex
from parrot.utils import tokenize
import parrot.utils.files as files

class Processor(object):
    def __init__(self, api, logger,
                 language="portuguese",
                 use_database=True,
                 just_tokenize=False):
        self.api = api
        self.logger = logger
        self.use_database = use_database
        self.just_tokenize = just_tokenize
        self.aborted_tweets = []

        tokenize = TweetTokenizer(preserve_case=True, reduce_len=True).tokenize
        self.stopwords = stopwords.words(language)
        self.spelling_replacer = replacers.SpellingReplacer()
        patterns = files.get_contractions(language)
        self.expand_contractions = replacers.RegexpReplacer(patterns, has_flags=True).replace

        # Named Entities Recognition
        self._ne_recognition = self._ne_training("paramopama+second_harem")

        try:
            self.pos_neg_influence = files.get_classifier('pos_neg')
            self.does_influence = files.get_classifier('does_influence')
        except Exception as e:
            self.logger.warning(e)

    def process_list(self, status_list):
        processed_texts = []
        tokens_list = []
        for status in status_list:
            try:
                tokens, p_text = self.process_tweet(status)
                processed_texts.append(p_text)
                tokens_list.append(tokens)
            except t_ex.AbortTweet as aborted_ex:
                err_status = aborted_ex.args[0]
                if err_status is not None:
                    self.aborted_tweets.append(err_status)
                    error_msg = "Aborting tweet %d. Moving on to next one."
                    self.logger.error(error_msg, err_status.id)
                continue
        self.logger.info("All tweets processed.")
        return (tokens_list, processed_texts)

    def process_tweet(self, status):
        text = None
        if self.use_database:
            status_dao = db.StatusDao()
            if status_dao.exists(status.id):
                self.logger.debug("Tweet %d already is in database.", status.id)
                raise t_ex.AbortTweet(None)
        try:
            #Replace spelling errors
            text = self.spelling_replacer.check(text)
        except t_ex.AbortTweet:
            raise t_ex.AbortTweet(status)
        #Tokenize text
        tokens = []
        for word in tokenize(text):
            if not (self.just_tokenize or regex_bank.is_punctuation(word)):
                tokens.append(word.strip().lower())
        processed_text = " ".join(tokens)
        if self.use_database:
            self._send_to_database(status, status_dao, tokens, processed_text)
        return (tokens, processed_text)

    def process(self, text):
        if self.does_influence is None or self.pos_neg_influence is None:
            raise Exception("Classifiers not defined")
        try:
            text = self.preprocess(text)
        except t_ex.AbortTweet:
            return 0
        tokens = tokenize(text)

        if self.does_influence.classify(tokens) == 0:
            return 0

        influence = self.pos_neg_influence.classify(tokens)
        return influence

    def preprocess(self, text):
        """Expands contractions and fixes spelling errors."""
        if len(text.strip()) == 0:
            raise t_ex.AbortTweet()
        text = self.expand_contractions(text)
        text = self.spelling_replacer.replace(text)
        return text

    def clean_tokens(self, tokens):
        """Removes punctuation."""
        return [t for t in tokens if not regex_bank.is_punctuation(t)]

    def retokenize(self):
        status_dao = db.StatusDao()
        status_list = status_dao.list_status_text()
        status_size = len(status_list)
        current_status = 1
        begin = time.time()
        for (status_id, text) in status_list:
            print("Tweet %04d of %d" % (current_status, status_size))
            try:
                processed_text = self.clean_tokens(tokenize(self.preprocess(text)))
            except t_ex.AbortTweet:
                processed_text = ""
            current_status = current_status + 1
            status_dao.update_processed_text(status_id, processed_text)
        end = time.time()
        elapsed = end - begin
        if elapsed > 60:
            print("Total time: %f min" % (elapsed/60))
        else:
            print("Total time: %fs" % elapsed)
        print("Average time: %fms/tweet" % (1000*elapsed/status_size))

    def tokens_list(self):
        status_dao = db.StatusDao()
        processed_texts = status_dao.list_status_processed_text()
        return [tokenize(text) for (_, text) in processed_texts]

    def _tag(self, text):
        tokens = tokenize(text)
        return self._ne_recognition.tag(tokens)

    def _ne_training(self, corpus_name):
        split = lambda line: tuple(line.strip().split('\t', 1))
        corpus = files.read_corpus(corpus_name, split)
        return nltk.HiddenMarkovModelTagger.train(corpus)

    def _add_user(self, user_id):
        user_dao = db.UserDao()
        if user_dao.exists(user_id):
            return False
        try:
            user = self.api.GetUser(user_id=user_id)
        except TwitterError as tw_err:
            if tw_err.args[0][0]['code'] == 50:
                self.logger.warning("User %s not found. Skipping mention.", mentioned_user.screen_name)
                return False
            else:
                raise tw_err
        user_dao.insert(user)
        return True

    def _process_mentions(self, status):
        mention_dao = db.MentionDao()
        for mentioned_user in status.user_mentions:
            self._add_user(mentioned_user.id)
            try:
                mention_dao.insert(status.id, status.user.id, mentioned_user.id)
            except Exception as i_err:
                if str(i_err).find("duplicate") >= 0:
                    pass
                else:
                    raise i_err

    def _send_to_database(self, status, status_dao, tokens, processed_text):
        #Try adding user to the database
        user_id = status.user.id
        self._add_user(user_id)
        #Add status, hashtags and words to database
        status_dao.insert(status, processed_text)
        self._process_mentions(status)
        self._process_words(status, tokens)

    def _process_words(self, status, tokens):
        tokens_set = set(tokens)
        word_dao = db.WordDao()
        word_use_dao = db.WordUseDao()
        for word in tokens_set:
            is_hashtag = word.startswith("#")
            not_empty = word.strip() != ""
            is_not_stopword = word not in self.stopwords
            if not is_hashtag and not_empty and is_not_stopword:
                if not word_dao.exists(word):
                    word_dao.insert(word, is_ngram=False)
                word_use_dao.insert(status, word, tokens_set)


def get_retweet_chain(status):
    rt_chain = [status]
    original = status.retweeted_status
    while original is not None:
        rt_chain.append(original)
        original = original.retweeted_status
    return rt_chain
