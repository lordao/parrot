from enchant.tokenize import Filter, tokenize

import regex as re

import parrot.regex_bank as re_bank

class TwitterFilter(Filter):
    """Subclass of filter, responsible for filtering words that will not
    be checked for spelling. Overriding the '_skip' method is sufficient."""
    _pattern = re.compile(
        r"|".join([re_bank.re_emoticons(),
                    re_bank.re_twitter_specifics(),
                    re_bank.re_numerals()]),
        re.U)
    def _skip(self, word):
        """Method used by superclass's methods to define which words will
        be skipped during the spell check process. It uses the '_pattern'
        attribute as the filter re."""
        word = "".join(word.tolist())
        has_matches = False or self._pattern.findall(word)
        is_only_punctuation = not re_bank.strip_punctuation(word)
        return has_matches or is_only_punctuation

class TwitterTokenizer(tokenize):
    _pattern = re.compile(re_bank.re_status_tokenize(), re.U)
    def __next__(self):
        if self.offset is None:
            self.offset = 0
        text = "".join(self._text.tolist())
        match = self._pattern.search(text, self.offset)
        if match is None:
            raise StopIteration()
        start_pos = match.start()
        end_pos = match.end()
        self.offset = end_pos
        # Return if word isnt empty
        if start_pos < end_pos:
            return (self._text[start_pos:end_pos], start_pos)
        raise StopIteration()
