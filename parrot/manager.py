import logging

import twitter

from parrot.database import StatusDao
from parrot.extractor import Extractor
from parrot.processor import Processor
from parrot.utils.files import get_log

class Manager(object):
    def __init__(self,
                 tweets_file=None,
                 api_key="vDOu3Y4TMdY889K5qg9NQ",
                 api_secret="8suTzgNgKnPfaTarZzjcFJUAj4kjOKrCj2PdZnj4",
                 token_key="447569574-FRK2Ct1ye5t4aaLtE5ZQhqSss1H15FykRN6aaXsm",
                 token_secret="owLvdsTqsEuFKUx7f8DDLFapmWU0qzlRSKpHoLmX7vPVT"):
        logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(message)s")
        log_formatter = logging.Formatter("%(asctime)-15s %(levelname)-8s %(message)s")
        self.logger = create_logger("manager", log_formatter, logging.INFO)
        while True:
            try:
                self.api = connect(api_key, api_secret, token_key, token_secret)
            except twitter.TwitterError as tw_err:
                if tw_err.args[0][0]['code'] == 88:
                    self.logger.error("Limit exceeded! Waiting until next cycle...")
                    time.sleep(API_GET_TIMEOUT)
                    count = 180
                else:
                    self.logger.error("Unknown error:\n" + tw_err)
                    break
            else:
                self.logger.info("API initialized!")
                break

        ex_logger = create_logger("extractor", log_formatter, logging.DEBUG) #Extractor logging
        pr_logger = create_logger("processor", log_formatter, logging.DEBUG) #Processor logging

        self.extractor = Extractor(self.api, ex_logger, tweets_file)
        self.processor = Processor(self.api, pr_logger)

    def fetch_daemon(self, screen_name, count=20, tries=None):
        self.extractor.daemon(screen_name, count, tries)

    def process_current_list(self):
        self.processor.process_list(self.extractor.tweets.values())

def connect(api_key, api_secret, token_key, token_secret):
    return twitter.Api(consumer_key=api_key,
                       consumer_secret=api_secret,
                       access_token_key=token_key,
                       access_token_secret=token_secret)

def create_logger(name, formatter, logging_level):
    logger = logging.getLogger(name)
    file_handler = logging.FileHandler(get_log(name))
    file_handler.setLevel(logging_level)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging_level)
    formatter = logging.Formatter("%(asctime)-15s %(levelname)-8s %(message)s")
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
    return logger
