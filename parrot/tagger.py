from nltk.corpus import floresta
from nltk.tag import DefaultTagger, UnigramTagger, BigramTagger, TrigramTagger

class Tagger(object):
    def __init__(self, max_sents=6000):
        self._train_sents = floresta.tagged_sents()[:max_sents]
        self._max_sents = max_sents
        self._tagger = _gen_tagger(self._train_sents, backoff=DefaultTagger("SUBJ>"))

    def set_train_sents(self, max_sents):
        self._max_sents = max_sents
        self._train_sents = floresta.tagged_sents()[:max_sents]
        _gen_tagger(self._train_sents)

    def evaluate(self):
        return self._tagger.evaluate(floresta.tagged_sents()[self._max_sents:])

    def tag_batch(self, tokens_list):
        return [self._tagger.tag(tokens) for tokens in tokens_list]

def _gen_tagger(train_sents, tagger_classes=None, backoff=None):
    if tagger_classes is None:
        tagger_classes = [UnigramTagger, BigramTagger, TrigramTagger]
    for cls in tagger_classes:
        backoff = cls(train_sents, backoff=backoff)
    return backoff
