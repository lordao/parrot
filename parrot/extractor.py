# -*- coding: utf-8 -*-

import pickle
import sys
import time

import twitter

API_GET_TIMEOUT = 915

class Extractor(object):
    def __init__(self,
                 api,
                 logger,
                 tweets_file=None):
        self.api = api
        self.max_id = None
        self.since_id = None
        self.logger = logger
        if tweets_file is None:
            self.tweets = {}
            self.since_id_counter = 0
        else:
            self.load_tweets(tweets_file)
            self.since_id_counter = self.highest_id()

    def daemon(self, screen_name, count=20, tries=None):
        """Runs a "daemon" of sorts, downloading new tweets.
        Parameters:
            screen_name: Name of the user to download tweets from
            count: Amount of tweets to be downloaded
            tries: Amount of attempts before a timeout. """
        if tries is None:
            tries = 5
        while True:
            old_len = self.num_tweets()
            self.logger.debug("Fetching tweets...")
            tweets = self.fetch_tweets(screen_name, count)
            self.logger.debug("Fetched!")
            self.add_tweets(tweets)
            diff = self.num_tweets() - old_len
            if diff == 0:
                tries -= 1
                self.logger.error("Nenhum tweet novo. Tentativas restantes: %d de 5.", tries)
            else:
                self.logger.info("Baixados %d tweets novos.", diff)
            if tries == 0:
                self.logger.error("Não consegui baixar mais tweets. Parando...")
                break


    def fetch_tweets(self, user, count=20):
        tweets = self.api.GetUserTimeline(
            screen_name=user,
            count=count,
            max_id=self.max_id,
            since_id=self.since_id,
            trim_user=False,
            exclude_replies=False,
            include_rts=True)
        if not tweets:
            if self.since_id_counter > 0:
                self.logger.debug(
                    "Tweaking. ID: %s - MAX: %s - MIN: %s",
                    str(self.max_id),
                    str(self.since_id),
                    self.since_id_counter)
                self.since_id_counter = 0
                self.since_id = None
            self.max_id = None
        else:
            older = tweets[-1]
            self.max_id = older.id - 1
            #newer = tweets[0]
            #self.since_id = self.next_since_id(user, newer)
        self.since_id_counter += 1
        return tweets

    def _update_tweets_version(self):
        """Since there may be some differences in attributes in newer versions of
        the python-twitter module, this function tries to update the current tweets
        database, *supposedly* making library upgrades seamless.

        For now, there is no way to test if this will work on the long run."""
        new_tweets = {}
        not_found = []
        count = 180
        retries = 10
        stdout = sys.stdout
        tweet_ids = list(self.tweets.keys())
        tweet_count = len(tweet_ids)
        cur_tweet = 1
        nf_count = 0
        out_template = "\rProcessing tweet %d of %d. Not found: %d."
        for tweet_id in tweet_ids:
            stdout.write(out_template % (cur_tweet, tweet_count, nf_count))
            stdout.flush()
            if count == 0:
                self.logger.debug("Limit exceeded! Waiting for 15 minutes...")
                time.sleep(API_GET_TIMEOUT)
                count = 180
                self.logger.debug("And we're back!")
            while True:
                try:
                    tweet = self.api.GetStatus(id=tweet_id, include_entities=True)
                except twitter.TwitterError as tw_err:
                    if tw_err.args[0][0]['code'] == 88:
                        self.logger.error("Limit exceeded! Waiting until next cycle...")
                        retries -= 1
                        if retries < 0:
                            self.logger.error("Something went wrong, dumping API object.")
                            pickle.dump(self.api, open('./api_dump.pickle', 'wb'))
                            raise Exception({'count': count, 'tweet_id': tweet_id})
                        time.sleep(API_GET_TIMEOUT)
                        count = 180
                    else:
                        self.logger.error("Tweet id %d was not found!" % tweet_id)
                        not_found.append(tweet_id)
                        nf_count += 1
                        break
                else:
                    self.logger.info("Downloaded tweet #%d." % tweet_id)
                    new_tweets[tweet_id] = tweet
                    break
            count -= 1
            cur_tweet += 1
        not_found = (not_found, self.tweets)
        self.tweets = new_tweets
        return not_found

    def add_tweets(self, tweets):
        """Adds a list of tweets to the object's dictionary of tweets.
        Duplicates won't be added, a debug message will be recorded by
        the logger instead."""
        for i in range(len(tweets)):
            tweet = tweets[i]
            tid = tweet.id
            if tid in self.tweets:
                self.logger.debug(
                    "Duplicate: ID: %s. MAX: %s - MIN: %s - COUNT: %s",
                    tid,
                    str(self.max_id),
                    str(self.since_id),
                    self.since_id_counter)
            else:
                self.tweets[tid] = tweet

    def highest_id(self):
        return max(self.tweets.keys())

    def lowest_id(self):
        return min(self.tweets.keys())

    def get_debug_dict(self):
        return {"max_id": self.max_id,
                "since_id": self.since_id,
                "since_id_counter": self.since_id_counter}

    def num_tweets(self):
        return len(self.tweets)

    def next_since_id(self, user, most_recent):
        pass

    def load_tweets(self, tweets_file):
        self.tweets = pickle.load(open(tweets_file, 'rb'))
        self.since_id_counter = self.highest_id()

    def dump_tweets(self, tweets_file):
        pickle.dump(self.tweets, open(tweets_file, 'wb'))
