import dill
import regex as re

from os.path import abspath, dirname, join
MAIN_DIRECTORY = abspath(dirname(dirname(dirname(__file__))))

def get_full_path(*path):
    return join(MAIN_DIRECTORY, *path)

def get_contractions_path(language):
    return get_full_path("res", "contractions", language + ".txt")

def get_log(log_name):
    return get_full_path("logs", log_name + ".log")

def get_wordlist(dict_name):
    return get_full_path("res", "wordlists", dict_name + ".txt")

def get_corpus(corpus_name):
    return get_full_path("res", "corpora", corpus_name + ".txt")

def get_classifier_path(name):
    return get_full_path("res", "classifiers", name + ".pickle")

def dump_classifier(name, classifier):
    if classifier.__class__.__name__ != "ParrotClassifier":
        raise Exception("ParrotClassifier must be provided")
    dill.dump(classifier, open(get_classifier_path(name), "wb"))

def get_classifier(name):
    return dill.load(open(get_classifier_path(name), "rb"))

def get_contractions(language="portuguese"):
    filepath = get_contractions_path(language)
    contr_list = []
    with open(filepath, "r", encoding="utf-8") as contractions_file:
        for line in contractions_file:
            pat, sub, flag = line.strip().split(", ")
            if flag == "I":
                contr_list.append((pat, sub, re.IGNORECASE))
            else:
                contr_list.append((pat, sub, None))
    return contr_list

def read_corpus(corpus_name, process_lines=None):
    if process_lines is None:
        process_lines = lambda x: x
    corpus = []
    with open(get_corpus(corpus_name), "r") as f:
        sent = []
        for line in f:
            if line.strip():
                sent.append(process_lines(line))
            else:
                if sent:
                    corpus.append(sent)
                sent = []
    return corpus
