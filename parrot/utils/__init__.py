# -*- coding: UTF-8 -*-
from nltk import TweetTokenizer
import itertools
from functools import reduce
from operator import itemgetter

NaN = float("NaN")

tokenize = TweetTokenizer(preserve_case=True, reduce_len=True).tokenize


def safe_div(dividend, divisor):
    if divisor == 0:
        return NaN
    return dividend / divisor


def string_matching_kmp(text='', pattern=''):
    """Returns positions where pattern is found in text.
    O(m+n)
    Example: text = 'ababbababa', pattern = 'aba'
        string_matching_knuth_morris_pratt(text, pattern) returns [0, 5, 7]
    @param text text to search inside
    @param pattern string to search for
    @return list containing offsets (shifts) where pattern is found inside text
    """
    n = len(text)
    m = len(pattern)
    offsets = []
    pi = compute_prefix_function(pattern)
    q = 0
    for i in range(n):
        while q > 0 and pattern[q] != text[i]:
            q = pi[q - 1]
        if pattern[q] == text[i]:
            q = q + 1
        if q == m:
            offsets.append(i - m + 1)
            q = pi[q-1]

    return offsets


def compute_prefix_function(p):
    m = len(p)
    pi = [0] * m
    k = 0
    for q in range(1, m):
        while k > 0 and p[k] != p[q]:
            k = pi[k - 1]
        if p[k] == p[q]:
            k = k + 1
        pi[q] = k
    return pi


def compose(*functions):
    return reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)


def group_use(count_list):
    """Given a list containing 3-uples of the form (status_id, word, count),
    it returns a dictionary that, for each status, has a dictionary mapping
    words to how many times they appeared on said status."""
    # Creates an iterator that will return the items as (status_id, [(status_id,word,count)])
    iterator = itertools.groupby(count_list, itemgetter(0, 1))
    my_list = []
    for ((status_id, impact_on_traffic), list_tp) in iterator:
        word_list = [(word, count) for _, _, word, count in list_tp]
        my_list.append((status_id, impact_on_traffic, dict(word_list)))
    return my_list


def zipWith(f, xs, ys):
    x_len = len(xs)
    y_len = len(ys)
    n = max(x_len, y_len)
    result = []
    for i in range(n):
        result.append(f(xs[i], ys[i]))
    return result


def concat(l):
    return [item for sublist in l for item in sublist]


def unzip(tuples):
    xs = []
    ys = []
    for (x, y) in tuples:
        xs.append(x)
        ys.append(y)
    return (xs,ys)


def fmap(f, tp):
    return (f(tp[0]), tp[1])


def map_inv(functions, obj):
    return [f(obj) for f in functions]


def fst(tp):
    return tp[0]

def snd(tp):
    return tp[1]
