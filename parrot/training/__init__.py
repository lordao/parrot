# -*- coding: UTF-8 -*-
# Python built-ins
import itertools
import math
import pickle

from abc import ABC, abstractmethod
from functools import partial
from random import shuffle

# Libraries
from cycler import cycler
import nltk
import matplotlib.pyplot as plt
from scipy import mean
import sklearn.metrics as sk_metrics
from sklearn.feature_extraction import DictVectorizer

# Parrot's libraries
import parrot.database as db
import parrot.replacers as replacers
import parrot.utils as utils

from parrot.regex_bank import is_punctuation


stemmer = nltk.RSLPStemmer()


class ParrotClassifier(ABC):
    def __init__(self, classifier, features, name=None, use_preprocessing=True, use_stemming=False):
        if not (hasattr(classifier, "classify") or hasattr(classifier, "predict")):
            raise Exception("Please provide a valid classifier")
        self.classifier = classifier
        if name is None:
            name = self.classifier.__class__.__name__
        self.name = name

        if features.__class__.__name__ != "list":
            raise Exception("Please provide a list of features")
        self.features = features
        self.preprocess = make_preprocess_function(use_preprocessing, use_stemming)

    def classify_many(self, tokens_list):
        return [self.classify(tokens) for tokens in tokens_list]

    @abstractmethod
    def classify(self, tokens):
        pass

    def plot_roc_curve(self, samples, actual_values):
        (x, y) = self.roc_point(samples, actual_values)
        xs = [0, x, 1]
        ys = [0, y, 1]
        roc_auc = sk_metrics.auc(xs, ys)
        plt.title('Receiver Operating Characteristic - %s' % str(self))
        plt.plot(xs, ys, 'r', label='AUC = %0.2f'% roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0, 1], [0, 1], '--', color='gray')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('Sensibilidade')
        plt.xlabel('1 - Especificidade')
        plt.grid()
        plt.show()

    def auc(self, samples, correct, metrics=None):
        (x, y) = self.roc_point(samples, correct, metrics)
        xs = [0, x, 1]
        ys = [0, y, 1]
        return sk_metrics.auc(xs, ys)

    def roc_point(self, samples, correct, metrics=None):
        """Returns the coordinates for the classifier's location on ROC space.
        Usage: classifier.roc_point(samples, correct, metrics=None)
        samples: known inputs for the classifier;
        correct: correct values for classification;
        matrics: avoids repeated computation if metrics have been calculated before."""
        if metrics is None:
            metrics = self.metrics(samples, correct)
        return (1-metrics["specificity"], metrics["sensibility"])

    def metrics(self, samples, correct):
        if len(samples) == 0:
            return None
        inferred = self.classify_many(samples)
        return self._metrics(inferred, correct)

    def _metrics(self, inferred, correct):
        true_pos = 0
        false_pos = 0
        true_neg = 0
        false_neg = 0
        for i in range(len(inferred)):
            if correct[i] > 0:
                if inferred[i] == correct[i]:
                    true_pos += 1
                else:
                    false_neg += 1
            else:
                if inferred[i] == correct[i]:
                    true_neg += 1
                else:
                    false_pos += 1
        pos = true_pos + false_neg
        neg = true_neg + false_pos
        total = len(inferred)
        inferred_pos = true_pos + false_pos
        inferred_neg = true_neg + false_neg
        metrics = {
            "accuracy": (true_pos + true_neg) / total,
            "sensibility": utils.safe_div(true_pos, pos),
            "specificity": utils.safe_div(true_neg, neg),
            "posPredictivity": utils.safe_div(true_pos, inferred_pos),
            "negPredictivity": utils.safe_div(true_neg, inferred_neg)
        }
        metrics["efficiency"] = (metrics["sensibility"] + metrics["specificity"]) / 2
        precision = metrics["precision"] = metrics["posPredictivity"]
        recall = metrics["recall"] = metrics["sensibility"]
        metrics["fmeasure"] = 2 * ( (precision*recall) / (precision + recall) )
        return metrics

    def __str__(self):
        return "%s (%d features)" % (self.name, len(self.features))


class ParrotNLTKClassifier(ParrotClassifier):
    def __init__(self, classifier, features, name=None, use_preprocessing=True, use_stemming=False):
        ParrotClassifier.__init__(self, classifier, features, name, use_preprocessing, use_stemming)

    def classify(self, tokens):
        if tokens.__class__ == dict:
            c_input = tokens
        else:
            tokens = " ".join(tokens)
            tokens = self.preprocess(tokens)
            c_input = apply_features(self.features, tokens)
        return self.classifier.classify(c_input)


class ParrotSKClassifier(ParrotClassifier):
    def __init__(self, classifier, features, vectorizer, name=None, use_preprocessing=True, use_stemming=False):
        ParrotClassifier.__init__(self, classifier, features, name, use_preprocessing, use_stemming)
        self.vectorizer = vectorizer

    def classify(self, tokens):
        if tokens.__class__ == dict:
            c_input = tokens
        else:
            c_input = apply_features(self.features, tokens)
        c_input = self.vectorizer.transform(c_input)
        return self.classifier.predict(c_input)[0]


class Trainer(object):
    def __init__(self, classifiers=None, cutoff_ratio=0.6, use_preprocessing=True, use_stemming=False):
        if classifiers is None:
            classifiers = [ nltk.NaiveBayesClassifier
                          , nltk.DecisionTreeClassifier
                          , nltk.MaxentClassifier
                          ]
        self.classifiers = classifiers
        self.cutoff_ratio = cutoff_ratio
        self.dataset = None
        self.vectorizer = DictVectorizer()
        self.sk_dataset = None
        self.features = None
        self.corpus = None
        self.dataset = None
        self.use_preprocessing = use_preprocessing
        self.use_stemming = use_stemming
        self._result = None

    def set_features(self, features):
        self.features = features
        return self

    def set_corpus(self, corpus):
        self.corpus = corpus
        if self.features is not None:
            self.set_dataset(corpus_to_dataset(self.features, corpus))
        return self

    def set_dataset(self, dataset):
        if dataset is None:
            raise Exception("Dataset can't be None.")
        self.dataset = dataset
        return self

    def train(self, dataset=None):
        if self.dataset and dataset or not self.dataset and not dataset:
            raise Exception("Please provide a dataset either as a parameter or with set_dataset.")
        if self.dataset is not None and dataset is None:
            dataset = self.dataset

        shuffle(dataset)
        cutoff = int(math.floor(len(dataset) * self.cutoff_ratio))
        train_set = dataset[:cutoff]
        test_set  = dataset[cutoff:]
        self._result = {}
        self._result['classifiers'] = [self._train(c, dataset) for c in self.classifiers]
        self._result['train_set'] = train_set
        self._result['test_set'] = test_set
        return self

    def _train(self, classifier, dataset):
        if classifier.__class__ == tuple:
            (classifier, name) = classifier
        else:
            name = None
        if hasattr(classifier, "train"):
            return ParrotNLTKClassifier(classifier.train(dataset), self.features, name, self.use_preprocessing, self.use_stemming)
        elif hasattr(classifier, "fit"):
            if self.sk_dataset is None:
                (samples, labels) = utils.unzip(dataset)
                samples = self.vectorizer.fit_transform(samples)
                self.sk_dataset = (samples, labels)
            else:
                (samples, labels) = self.sk_dataset
            if hasattr(classifier, "__call__"):
                sk = classifier().fit(samples, labels)
            else:
                sk = classifier.fit(samples, labels)
            return ParrotSKClassifier(sk, self.features, self.vectorizer, name, self.use_preprocessing, self.use_stemming)
        else:
            raise Exception("Invalid classifier!")

    def result(self):
        return self._result

    def evaluate(self):
        test_set = self._result['test_set']
        classifiers = self._result['classifiers']
        (features, actual_values) = utils.unzip(test_set)

        def _evaluate(classifier):
            hypothesis = classifier.classify_many(features)
            if hasattr(hypothesis, "tolist"):
                hypothesis = hypothesis.tolist()
            cm = nltk.ConfusionMatrix(actual_values, hypothesis)
            return { 'accuracy': nltk.classify.accuracy(classifier, test_set)
                   , 'classifier': classifier.name
                   , 'confusion-matrix': cm
                   , 'hypothesis-actual': (hypothesis, actual_values)
                   }

        return map(_evaluate, classifiers)

    def print_evaluation(self):
        for evaluation in self.evaluate():
            print(evaluation['classifier'])
            print(evaluation['confusion-matrix'].pretty_format())
            print("Accuracy: %.2f" % evaluation['accuracy'])


class Feature(object):
    def __init__(self, name, function):
        self.name = name
        self.function = function
        self.extract = self.__call__

    def __str__(self):
        return self.name

    def __call__(self, tokens):
        return (self.name, self.function(tokens))


def make_preprocess_function(use_preprocessing, use_stemming):
    preprocess = [utils.tokenize]
    if use_preprocessing:
        contractions = replacers.contractions_handler()
        preprocess.append(contractions)
    if use_stemming:
        preprocess.insert(0, stem_tokens)
    preprocess = utils.compose(*preprocess)
    return preprocess


def make_feature(ngram, idf_score=None):
    if ngram.__class__ == tuple:
        return has_ngram(ngram, idf_score)
    else:
        return has(ngram, idf_score)

def has(word, idf_score=None):
    return Feature("has(%s)" % word, partial(find_word, word, idf_score))


def find_word(word, idf_score, tokens):
    if word in tokens:
        if idf_score:
            return idf_score
        return 1
    return 0


def has_ngram(ngram, idf_score=None):
    f_name = "has_ngram%s" % str(ngram)
    start_anchor = "<s>" in ngram
    end_anchor = "</s>" in ngram
    if start_anchor or end_anchor:
        ngram = list(ngram)
    return Feature(f_name, partial(find_ngram, start_anchor, end_anchor, idf_score, ngram))

def find_ngram(start_anchor, end_anchor, idf_score, ngram, tokens):
    n = len(ngram)
    if start_anchor:
        n -= 1
    if end_anchor:
        n -= 1
    if start_anchor:
        if end_anchor:
            if ngram[1:][:-1] == tokens:
                if idf_score:
                    return idf_score
                return 1
        else:
            if ngram[1:] == tokens[:n]:
                if idf_score:
                    return idf_score
                return 1
    elif end_anchor:
        if ngram[:-1] == tokens[-n:]:
            if idf_score:
                return idf_score
            return 1
    else:
        if utils.string_matching_kmp(tokens, ngram) != []:
            if idf_score:
                return idf_score
            return 1
    return 0

def corpus_to_dataset(features, corpus):
    def fmap_feature(t_c):
        (tokens, category) = t_c
        sample = dict(utils.map_inv(features, tokens))
        return (sample, category)
    return list(map(fmap_feature, corpus))


def apply_features(features, tokens):
    return dict(utils.map_inv(features, tokens))


def evaluate(classifiers, test_set):
    (features, actual_values) = utils.unzip(test_set)

    def avg_error(hypothesis):
        error = utils.zipWith(lambda a, b: (a-b)**2, actual_values, hypothesis)
        return mean(error)

    def _evaluate(classifier):
        hypothesis = classifier.classify_many(features)
        return { 'accuracy': nltk.classify.accuracy(classifier, test_set)
               , 'error': avg_error(hypothesis)
               , 'confusion-matrix': nltk.ConfusionMatrix(actual_values, hypothesis)
               }

    return list(map(_evaluate, classifiers))


def roc_curves(classifiers, inputs, actual_values):
    plt.rc('lines', linewidth=2)
    plt.rc('axes', prop_cycle=(cycler('linestyle', ['-', '--', ':']) *
                               cycler('color', ['r', 'g', 'b', 'darkorange', 'c', 'y', 'm', 'k'])))
    plt.title('Receiver Operating Characteristic')
    plt.plot([0, 1], [0, 1], 'r--', color='gray')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.grid()
    for c in classifiers:
        predictions = c.classify_many(inputs)
        false_positive_rate, true_positive_rate, _ = sk_metrics.roc_curve(actual_values, predictions)
        roc_auc = sk_metrics.auc(false_positive_rate, true_positive_rate)
        plt.plot(false_positive_rate, true_positive_rate, label='%s = %0.2f'% (str(c), roc_auc))
    plt.legend(loc='lower right')
    plt.show()


def roc_several(trainers, inputs, actual_values):
    plt.rc('lines', linewidth=2)
    props = (cycler('linestyle', ['-', '--', ':'])
        * cycler('color', ['r', 'g', 'b', 'darkorange', 'c', 'y', 'm', 'k']))
    plt.title('Receiver Operating Characteristic')
    plt.plot([0, 1], [0, 1], 'r--', color='gray')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('Sensibilidade')
    plt.xlabel('1 - Especificidade')
    plt.grid()
    classifiers = group_classifiers(trainers)
    for ((name, cs), p) in zip(classifiers.items(), props):
        (xs, ys) = utils.unzip([(0, 0)] + sorted([c.roc_point(inputs, actual_values) for c in cs]) + [(1, 1)])
        plt.scatter(xs, ys, color=p["color"], s=10)
        area_under_curve = sk_metrics.auc(xs, ys, reorder=True)
        plt.plot(xs, ys, color=p["color"], label='%s = %0.2f'% (name, area_under_curve))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.legend(loc='lower right')
    plt.show()

def classifiers_metrics(trainers, inputs, actual_values, filename):
    classifiers = group_classifiers(trainers)
    points = {}
    labels = sorted(list(set(actual_values)))
    is_binary = len(labels) == 2
    for (name, cs) in classifiers.items():
        scores = {"precision": [], "recall": [], "f-measure": []}
        if is_binary:
            scores["ROC AUC"] = []
            scores["ROC POINT"] = []
        for c in cs:
            n_features = len(c.features)
            hypothesis = c.classify_many(inputs)
            (p, r, f, _) = sk_metrics.precision_recall_fscore_support(actual_values, hypothesis, labels=labels)
            (p_str, r_str, f_str) = ("  ".join([str(m) for m in ms]) for ms in [p, r, f])
            scores["precision"].append((n_features, p))
            scores["recall"].append((n_features, r))
            scores["f-measure"].append((n_features, f))
            if is_binary:
                roc = sk_metrics.roc_curve(actual_values, hypothesis)
                roc_auc = sk_metrics.roc_auc_score(actual_values, hypothesis)
                scores["ROC POINT"].append((n_features, roc))
                scores["ROC AUC"].append((n_features, roc_auc))
        points[name] = scores

    if filename is not None:
        pickle.dump(points, open(filename, "wb"))
    return points


def plot_metrics(trainers, inputs, actual_values, average_method="weighted", filename=None):
    classifiers = group_classifiers(trainers)
    points = {}
    for (name, cs) in classifiers.items():
        scores = {"precision": [], "recall": [], "f-measure": []}
        for c in cs:
            n_features = len(c.features)
            hypothesis = c.classify_many(inputs)
            (p, r, f, _) = sk_metrics.precision_recall_fscore_support(actual_values,
                                                                      hypothesis,
                                                                      average=average_method)
            scores["precision"].append((n_features, p))
            scores["recall"].append((n_features, r))
            scores["f-measure"].append((n_features, f))
#            if len(c.classifier.labels()) == 2:
#                if "ROC AUC" not in scores:
#                    scores["ROC AUC"] = []
#                roc_auc = sk_metrics.roc_auc_score(actual_values, hypothesis)
#                scores["ROC AUC"].append((n_features, roc_auc))
        points[name] = scores

    if filename is not None:
        pickle.dump(points, open(filename, "wb"))
    show_metrics_plots(points)


def show_metrics_plots(points):
    colors = itertools.cycle(["b", "g", "r", "y", "c", "darkorange", "m", "k"])
    linestyles = itertools.cycle(["solid", "dashed", "dotted"])

    data = {}
    for (name, scores) in points.items():
        for (score, ps) in scores.items():
            if score not in data:
                data[score] = {}
            data[score][name] = ps
    points = data

    for (score_name, classifier_score) in points.items():
        plt.figure()
        plt.rc("lines", linewidth=1)
        plt.title("Evolution with number of features")
        plt.xlabel("#Features")
        plt.ylabel(score_name.capitalize())
        plt.ylim([0, 1])
        plt.grid()
        classifier_score = classifier_score.items()
        classifier_score = sorted(classifier_score, key=lambda tp: tp[1][-1], reverse=True)
        for (classifier_name, points) in classifier_score:
            color = next(colors)
            linestyle = next(linestyles)
            (xs, ys) = utils.unzip(points)
            xmax = max(xs) + 5
            plt.xlim([0, xmax])
            plt.scatter(xs, ys, s=10, color=color)
            plt.plot(xs, ys, label=classifier_name, color=color, linestyle=linestyle)
            plt.legend(loc="lower right")
    plt.show()

def plot_file(filename):
    points = pickle.load(open(filename, "rb"))
    show_metrics_plots(points)

def group_classifiers(trainers):
    cs = sorted([c for t in trainers for c in t.result()['classifiers']], key=lambda c: c.name)
    dic = {}
    last_name = ""
    for c in cs:
        if last_name != c.name:
            last_name = c.name
            dic[last_name] = []
        dic[last_name].append(c)
    return dic


def choose_words(n=None, use_preprocessing=True):
    fst = lambda tp: tp[0]
    snd = lambda tp: tp[1]
    pick_words = lambda doc: map(fst, sorted(doc.items(), reverse=True, key=snd)[:n])

    words_list = map(pick_words, tf_idf(use_preprocessing)[2])
    words_list = [word for document_words in words_list for word in document_words]
    words_list.append("colisão")
    return list(set(words_list))

def get_words(use_preprocessing, use_stemming):
    (tf, idf, tfIdf) = tf_idf(use_preprocessing, use_stemming)
    words = []
    for doc in tfIdf:
        ws = sorted(doc.items(), key=lambda tp: tp[1], reverse=True)
        ws = [tp[0] for tp in ws]
        words.append(ws)
    return (idf, words)

def stem_tokens(tokens):
    tokens_ = []
    for token in tokens:
        if "_" not in token:
            token = stemmer.stem(token)
        tokens_.append(token)
    return tokens_


def tf_idf(use_preprocessing=True, use_stemming=False):
    def valid(ngram):
        n = len(ngram)
        remove_punctuation = [g for g in ngram if not is_punctuation(g)]
        if remove_punctuation == []:
            return False
        return "<s>" not in ngram[1:] and "</s>" not in ngram[:n-1]

    s = db.StatusDao()
    texts = s.get_category_texts()
    freq_dists = []
    for text in texts.values():
        preprocess = make_preprocess_function(use_preprocessing, use_stemming)
        text = utils.concat([preprocess(tweet) for tweet in text])
        ngrams = [filter(valid, nltk.ngrams(text, n)) for n in [2, 3]]
        text = [token for token in text if not is_punctuation(token)]
        text.extend(utils.concat(ngrams))
        freq_dists.append(nltk.FreqDist(text))

    lengths = [len(fd) for fd in freq_dists]
    tf = []
    idf = {}
    n = len(texts)
    for i in range(n):
        fd = freq_dists[i]
        length = lengths[i]
        tf.append({})
        for (word, frequency) in fd.items():
            tf[i][word] = frequency/float(length)
            if word not in idf:
                idf[word] = 1.0
            else:
                idf[word] += 1
    idf = dict([(word, math.log(n/count)) for (word, count) in idf.items()])

    tfIdf = []
    for i in range(len(tf)):
        tfIdf.append({})
        for (word, frequency) in freq_dists[i].items():
            tfIdf[i][word] = tf[i][word] * idf[word]

    return (tf, idf, tfIdf)

