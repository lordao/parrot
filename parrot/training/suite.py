import re

import nltk
from nltk.classify.scikitlearn import SklearnClassifier
from parrot.training import *
import parrot.utils as utils
import pathos.multiprocessing as mp
import logging
import sklearn.naive_bayes as sk_nb
import sklearn.ensemble as sk_ens
import sklearn.svm as sk_svm
import sklearn.tree as sk_tree
import sklearn.neural_network as sk_nn

function_name_re = re.compile(r'train_\w+')

def show_results(file):
    results = pickle.load(open(file, "rb"))
    for (name, scores) in results.items():
        print(name)
        for metric in ['precision', 'recall', 'f-measure']:
            print(metric)
            for (_, score) in scores[metric]:
                print(",".join([str(s) for s in score]))
            print()
    return results

def apply_training_params(tp):
    (train_function, pre, stem) = tp
    f_name = function_name_re.findall(str(train_function))[0]
    t_id = "Function: {}, Preprocess: {}, Stem: {}".format(f_name, pre, stem)
    print("Training => {}".format(t_id))
    (ts, tweets) = utils.unzip(train_function(range(20, 140, 10), pre, stem))
    (X, Y) = utils.unzip(tweets[0])
    filename = "trainers-big-test-{}-{}preprocess-{}stem.pickle".format(f_name, "no-" if not pre else "", "no-" if not stem else "")
    print("Measuring => {}".format(t_id))
    metrics = classifiers_metrics(ts, X, Y, filename)
    print("Done! {}".format(t_id))
    return (ts, metrics)

def parallell_training():
    params = [(train, use_preprocessing, use_stem)
              for train in [train_many_all_vs_all, train_many_does_influence, train_many_pos_neg]
              for use_preprocessing in [False, True]
              for use_stem in [False, True]]
    tss = []
    mss = []
    p = mp.Pool(4)
    for (ts, metrics) in p.map(apply_training_params, params):
        tss.append(ts)
        mss.append(metrics)
    return (tss, mss)


def train(number_of_words, to_binary_function, use_preprocessing, use_stemming, auto_train=False):
    preprocess = make_preprocess_function(use_preprocessing, use_stemming)
    tweets = db.StatusDao().get_tweets()
    tweets = [(preprocess(t), c) for (t, c) in tweets]
    tweets = to_binary_function(tweets)

    (_, documents) = get_words(use_preprocessing, use_stemming)

    features = [make_feature(term) for doc in documents for term in doc[:number_of_words]]

    trainer = Trainer(classifiers=[
        (nltk.NaiveBayesClassifier, "Naïve Bayes (NLTK)"),
        (SklearnClassifier(sk_nb.BernoulliNB()), "Bernoulli Naïve Bayes (sklearn)"),
        (SklearnClassifier(sk_nb.MultinomialNB()), "Multinomial Naïve Bayes (sklearn)"),
        (SklearnClassifier(sk_svm.LinearSVC()), "SVC (linear kernel - liblinear) (sklearn)"),
        (SklearnClassifier(sk_svm.LinearSVC(class_weight="balanced")), "Balanced SVC (linear kernel - liblinear) (sklearn)"),
        (nltk.DecisionTreeClassifier, "Decision Tree (NLTK)"),
        (SklearnClassifier(sk_tree.DecisionTreeClassifier()), "Decision Tree (sklearn)"),
        (SklearnClassifier(sk_tree.DecisionTreeClassifier(class_weight="balanced")), "Balanced Decision Tree (sklearn)"),
        (SklearnClassifier(sk_ens.AdaBoostClassifier(n_estimators=50)), "AdaBoost (50 estimators) (sklearn)"),
        (SklearnClassifier(sk_ens.AdaBoostClassifier(n_estimators=100)), "AdaBoost (100 estimators) (sklearn)"),
        (SklearnClassifier(sk_ens.RandomForestClassifier(n_estimators=100, n_jobs=-1)), "Random Forest (100 estimators) (sklearn)"),
        (SklearnClassifier(sk_ens.RandomForestClassifier(n_estimators=100, class_weight="balanced", n_jobs=-1)), "Balanced Random Forest (100 estimators) (sklearn)")
    ], use_stemming=use_stemming).set_features(features).set_corpus(list(tweets))
    if auto_train:
        trainer.train()
    return (trainer, tweets)

def train_all_vs_all(use_preprocessing, use_stemming, number_of_words=20, auto_train=False):
    return train(number_of_words, lambda tweets: tweets, use_preprocessing, use_stemming, auto_train)

def train_pos_neg(use_preprocessing, use_stemming, number_of_words=20, auto_train=False):
    return train(number_of_words, lambda tweets: [t for t in tweets if t[1] != 0], use_preprocessing, use_stemming, auto_train)

def train_does_influence(use_preprocessing, use_stemming, number_of_words=20, auto_train=False):
    return train(number_of_words, lambda ts: [(t, 1 if c < 0 else c) for (t, c) in ts], use_preprocessing, use_stemming, auto_train)

def train_many(range, train_function, use_preprocessing=True, use_stemming=False):
    return [train_function(use_preprocessing, use_stemming, number_of_words, auto_train=True) for number_of_words in range]

def train_many_all_vs_all(range, use_preprocessing=True, use_stemming=False):
    return train_many(range, train_all_vs_all, use_preprocessing, use_stemming)

def train_many_does_influence(range, use_preprocessing=True, use_stemming=False):
    return train_many(range, train_does_influence, use_preprocessing, use_stemming)

def train_many_pos_neg(range, use_preprocessing=True, use_stemming=False):
    return train_many(range, train_pos_neg, use_preprocessing, use_stemming)
