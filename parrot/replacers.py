import enchant
import logging
import regex as re
import parrot.spelling as spelling

from enchant.tokenize import EmailFilter, URLFilter
from enchant.checker import SpellChecker
from nltk.metrics import edit_distance

import parrot.utils.files as files


class SpellingReplacer:
    def __init__(self, dict_name="pt_BR", max_dist=2):
        personal_word_list_path = files.get_wordlist(dict_name)
        self.spell_dict = enchant.DictWithPWL(dict_name, personal_word_list_path)
        self.max_dist = max_dist
        logging_level = logging.DEBUG
        self.logger = logging.getLogger("spelling_replacer")
        self.logger.setLevel(logging_level)
        #Print debug to file
        fh = logging.FileHandler(files.get_log("spelling_replacer"))
        fh.setLevel(logging_level)
        fh.setFormatter(logging.Formatter("%(asctime)-15s %(levelname)-8s %(message)s"))
        self.logger.addHandler(fh)

    def check(self, text, interactive=False):
        enchant_tokenizer = EmailFilter(URLFilter(spelling.TwitterFilter(spelling.TwitterTokenizer)))
        checker = SpellChecker(self.spell_dict, text, tokenize=enchant_tokenizer)
        for error in checker:
            error.replace(self.replace(error))
        return checker.get_text()

    def replace(self, word, status_id=None):
        if self.spell_dict.check(word):
            return word
        suggestions = self.spell_dict.suggest(word)
        if suggestions and edit_distance(word, suggestions[0]) <= self.max_dist:
            self.logger.info("Word: %s; Replacement: %s", word, suggestions[0])
            return suggestions[0]
        else:
            return word

class RegexpReplacer:
    def __init__(self, patterns, has_flags=False):
        self.patterns = []
        for tp in patterns:
            pattern = tp[0]
            repl = tp[1]
            flags = re.UNICODE
            if has_flags:
                flag = tp[2]
                if flag is not None:
                    flags = flags|flag
            try:
                self.patterns.append((re.compile(pattern, flags), repl))
            except re._regex_core.error:
                print(repr(pattern))
                print(repr(repl))

    def replace(self, text):
        s = text
        for (pattern, repl) in self.patterns:
           (s, count) = re.subn(pattern, repl, s)
        return s

class RepeatReplacer:
    def __init__(self, dictionary):
        self._repeat_regex = re.compile(r'(\w*)(\w)\2(\w*)', re.UNICODE)
        self._repl = r'\1\2\3'
        self._dict = dictionary

    def replace(self, word):
        if self._dict.check(word):
            return (word, True)
        repl_word = self._repeat_re.sub(self._repl, word)
        if repl_word != word:
            return self.replace(repl_word)
        else:
            return (repl_word, False)


def contractions_handler(language="portuguese"):
    patterns = files.get_contractions(language)
    return RegexpReplacer(patterns, has_flags=True).replace
