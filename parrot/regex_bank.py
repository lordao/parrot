# -*- coding: UTF-8 -*-
import string

PUNCTUATION = set(string.punctuation)

def is_punctuation(word):
    return word in PUNCTUATION

def strip_punctuation(word):
    return [ch for ch in word if not is_punctuation(ch)]

def re_numerals():
    return r"|".join([
        r"(\d+[ªº\xb0])",
        r"(\d{1,2}:\d{2})",
        r"((?:R\$\s)?\d+[,.]\d+)",
        r"(\d{1,2}(?:h|min|s))",
        r"(\d+)"])

def re_status_tokenize():
    return "|".join([re_urls(), re_twitter_specifics(), re_emoticons(), re_numerals(), re_words()])

def re_urls():
    return r"(http\S+)"

def re_twitter_specifics():
    return r"([@#]\w+)"

def re_emoticons():
    return "|".join([
        r"([;:=][\(\)/\\D])", #;( ;) ;/ ;\ ;D :( :) :/ :\ :D =( =) =/ =\ =D
        r"([\(\)/\\D][;:=])", #(; ); /; \; D; (: ): /: \: D: (= )= /= \= D=
        r"(\*_+\*)", #*_* *__* ...
        r"([xX]D)" #xD XD
        ])

def re_words():
    return r"(\w+-\w+)|(\w+'\w+)|(\w+)|([^\w\s]+)"

