from os.path import expandvars

import psycopg2
import psycopg2.extensions

from psycopg2.extras import DictCursor

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

CONNECTION_STRING = expandvars("dbname=parrot user=$PARROT_DB_USER password=$PARROT_DB_PASSWORD")


class CategoryDao(object):
    def __init__(self):
        self.insert_suggestion_query = """
            INSERT INTO category_suggestion (
                status_id,
                category_id,
                origin_ip
            ) VALUES (
                %(status_id)s,
                %(category_id)s
                %(origin_ip)s
            );"""
        self.insert_inferred_query = """
            INSERT INTO inferred_classifications (
                status_id,
                impact
            ) VALUES (
                %(status_id)s,
                %(impact)%s
            );"""

    def list_categories(self):
        with psycopg2.connect(CONNECTION_STRING, cursor_factory=DictCursor) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("""SELECT
                                id, name, impact_on_traffic
                            FROM category
                            WHERE impact_on_traffic != 0;""")
                    categories = [dict(list(row.items())) for row in cur.fetchall()]
                except:
                    categories = []
        conn.close()
        return categories

    def insert_inferred(self, status_id, impact):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = { 'status_id': status_id, 'impact': impact }
                cur.execute(self.insert_inferred_query, vals)
            conn.commit()
        conn.close()

    def insert_suggestion(self, status_id, category_id, origin_ip):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = { 'status_id': status_id, 'category_id': category_id, 'origin_ip': origin_ip }
                try:
                    cur.execute(self.insert_suggestion_query, vals)
                except psycopg2.IntegrityError as i_err:
                    raise Exception("Duplicate key!", i_err)
            conn.commit()
        conn.close()

class UserDao(object):
    def __init__(self):
        self.select_query = "SELECT * FROM users WHERE id = %(id)s;"
        self.exists_query = "SELECT 1 FROM users WHERE id = %(id)s;"
        self.insert_query = """INSERT INTO users (
                                    id
                                  , screen_name
                                  , name
                                  , statuses_count
                                  , friends_count
                                  , followers_count
                                  , created_at
                                  , lang
                                  , description
                                  , location
                                  , verified
                                  , favorites_count
                                  , geo_enabled
                                  , time_zone
                                  , protected
                                  , listed_count
                                  , url
                              ) VALUES (
                                    %(id)s
                                  , %(screen_name)s
                                  , %(name)s
                                  , %(statuses_count)s
                                  , %(friends_count)s
                                  , %(followers_count)s
                                  , %(created_at)s
                                  , %(lang)s
                                  , %(description)s
                                  , %(location)s
                                  , %(verified)s
                                  , %(favorites_count)s
                                  , %(geo_enabled)s
                                  , %(time_zone)s
                                  , %(protected)s
                                  , %(listed_count)s
                                  , %(url)s
                              );"""

    def insert(self, user):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(user)
                try:
                    cur.execute(self.insert_query, vals)
                except psycopg2.IntegrityError as i_err:
                    raise Exception("Duplicate key!", i_err)
            conn.commit()
        conn.close()

    def exists(self, user_id):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"id": user_id}
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def _get_dictionary(self, user):
        return {"id": user.id,
                "screen_name": user.screen_name,
                "name": user.name,
                "statuses_count": user.statuses_count,
                "friends_count": user.friends_count,
                "followers_count": user.followers_count,
                "created_at": user.created_at,
                "lang": user.lang,
                "description": user.description,
                "location": user.location,
                "verified": user.verified,
                "favorites_count": user.favorites_count,
                "geo_enabled": user.geo_enabled,
                "time_zone": user.time_zone,
                "protected": user.protected,
                "listed_count": user.listed_count,
                "url": user.url
               }

class StatusDao(object):
    def __init__(self):
        self.exists_query = "SELECT 1 FROM status WHERE id = %(id)s;"
        self.insert_query = """INSERT INTO status (
                                    id
                                  , user_id
                                  , text
                                  , processed_text
                                  , created_at
                                  , favorite_count
                                  , retweet_count
                                  , in_reply_to_user_id
                                  , in_reply_to_status_id
                              ) VALUES (
                                    %(id)s
                                  , %(user_id)s
                                  , %(text)s
                                  , %(processed_text)s
                                  , %(created_at)s
                                  , %(favorite_count)s
                                  , %(retweet_count)s
                                  , %(in_reply_to_user_id)s
                                  , %(in_reply_to_status_id)s
                               );"""
    def fetch(self, status_id):
        result = None
        query = "SELECT text, processed_text FROM status WHERE id = %(id)s;"
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"id": status_id}
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def exists(self, status_id):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"id": status_id}
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def insert(self, status, processed_text):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(status, processed_text)
                cur.execute(self.insert_query, vals)
            conn.commit()
        conn.close()

    def update_processed_text(self, status_id, processed_text):
        query = "UPDATE status SET processed_text = %(processed_text)s WHERE id = %(status_id)s;"
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"processed_text": processed_text, "status_id": status_id}
                cur.execute(query, vals)
            conn.commit()
        conn.close()

    def list_status_texts(self, has_category=False):
        not_cond = ""
        field = ""
        if has_category:
            not_cond = "NOT"
            field = ", category_id"
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("""SELECT
                                    s.id, text, processed_text, name %s
                                FROM status s
                                    INNER JOIN users u ON (s.user_id = u.id)
                                WHERE category_id IS %s NULL
                                ORDER BY s.id;""" % (field, not_cond))
                    pr_txts = cur.fetchall()
                except:
                    pr_txts = []
        conn.close()
        return pr_txts

    def list_status_text(self):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                status_txts = []
                try:
                    cur.execute("SELECT id, text FROM status;")
                    status_txts = cur.fetchall()
                except:
                    status_txts = []
        conn.close()
        return status_txts

    def list_status_id(self):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("SELECT id FROM status;")
                    status_list = cur.fetchall()
                except:
                    status_list = []
        conn.close()
        return status_list

    def list_status_processed_text(self):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("SELECT id, processed_text FROM status;")
                    pr_txts = cur.fetchall()
                except:
                    pr_txts = []
        conn.close()
        return pr_txts

    def get_tweets(self, processed=False):
        texts = []
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                query = """
                    SELECT
                        %s AS text,
                        impact_on_traffic
                    FROM status s
                        INNER JOIN category c ON (c.id = s.category_id)
                    WHERE category_id IS NOT NULL
                      AND %s IS NOT NULL
                """
                if processed:
                    query = query % ("processed_text", "processed_text")
                else:
                    query = query % ("text", "text")
                try:
                    cur.execute(query)
                    texts = cur.fetchall()
                except:
                    pass
        conn.close()
        return texts

    def get_category_texts(self, processed=False):
        texts = {-1: [], 0: [], 1: []}
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                query = """
                    SELECT
                        '<s>' || %s || '</s>' as text,
                        impact_on_traffic
                    FROM status s
                        INNER JOIN category c ON (c.id = s.category_id)
                    WHERE category_id IS NOT NULL
                    ORDER BY impact_on_traffic;
                """
                if processed:
                    query = query % "processed_text"
                else:
                    query = query % "text"
                try:
                    cur.execute(query)
                    for (text, impact) in cur:
                        texts[impact].append(text)
                except Exception as e:
                    print(e)
        conn.close()
        return texts

    def _get_dictionary(self, status, processed_text=None):
        vals = {"id": status.id,
                "user_id": status.user.id,
                "text": status.text,
                "processed_text": processed_text,
                "created_at": status.created_at,
                "favorite_count": status.favorite_count,
                "retweet_count": status.retweet_count,
                "in_reply_to_user_id": status.in_reply_to_user_id,
                "in_reply_to_status_id": status.in_reply_to_status_id
               }
        if processed_text is not None:
            vals["processed_text"] = processed_text
        return vals

class MentionDao(object):
    def __init__(self):
        self.insert_query = "INSERT INTO mentions (status_id, mentioning_id, mentioned_id) VALUES (%(status_id)s, %(mentioning_id)s, %(mentioned_id)s);"
        self.mentioning_query = "SELECT mentioned_id, status_id FROM mentions WHERE mentioning_id = %(mentioning_id)s;"
        self.mentioned_query = "SELECT mentioned_id, status_id FROM mentions WHERE mentioned_id = %(mentioned_id)s;"

    def insert(self, status_id, mentioning_id, mentioned_id):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"status_id": status_id,
                        "mentioning_id": mentioning_id,
                        "mentioned_id": mentioned_id}
                cur.execute(self.insert_query, vals)
            conn.commit()

    def mentioned_by(self, mentioned_id):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"mentioned_id": mentioned_id}
                cur.execute(self.mentioned_query, vals)
                result = cur.fetchall()
        conn.close()
        return result

    def mentions(self, mentioning_id):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"mentioning_id": mentioning_id}
                cur.execute(self.mentioning_query, vals)
                result = cur.fetchall()
        conn.close()
        return result

class HashtagDao(object):
    def __init__(self):
        self.select_query = "SELECT * FROM hashtag WHERE tag = %(tag)s;"
        self.exists_query = "SELECT 1 FROM hashtag WHERE tag = %(tag)s;"
        self.insert_query = "INSERT INTO hashtag (tag) VALUES (%(tag)s);"

    def exists(self, hashtag):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(hashtag)
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def insert(self, hashtag):
        if self.exists(hashtag):
            return
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(hashtag)
                cur.execute(self.insert_query, vals)
            conn.commit()

    def _get_dictionary(self, hashtag):
        return {"tag": hashtag}

class HashtagUseDao(object):
    def __init__(self):
        self.select_query = """SELECT status_id,
                                      hashtag_id,
                                      count
                                 FROM hashtag_use, hashtag
                                WHERE hashtag_id = id
                                  AND tag = %(tag)s
                                  AND status_id = %(status_id)s;"""
        self.exists_query = """SELECT 1
                                 FROM hashtag_use, hashtag
                                WHERE hashtag_id = id
                                  AND tag = %(tag)s
                                  AND status_id = %(status_id)s;"""
        self.insert_query = """INSERT INTO hashtag_use (status_id, hashtag_id, count)
                            SELECT %(status_id)s, id, %(count)s FROM hashtag WHERE tag = %(tag)s;"""

    def exists(self, status, hashtag):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"status_id": status.id, "tag": hashtag}
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def insert(self, status, hashtag, count, use_status_id=False):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(status, hashtag, count, use_status_id=use_status_id)
                cur.execute(self.insert_query, vals)
            conn.commit()

    def hashtag_list(self):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("SELECT tag FROM hashtag;")
                    hashtag_list = cur.fetchall()
                except:
                    hashtag_list = []
        conn.close()
        return [tp[0] for tp in hashtag_list]

    def hashtag_list_by_status(self):
        query = """SELECT status_id, tag, count
                     FROM hashtag_use INNER JOIN hashtag h ON (h.id = hashtag_id)
                   ORDER BY status_id;"""
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(query)
                    hashtag_list = cur.fetchall()
                except:
                    hashtag_list = []
        conn.close()
        return hashtag_list

    def _get_dictionary(self, status, hashtag, count=None, use_status_id=False):
        if use_status_id:
            status_id = status
        else:
            status_id = status.id
        dic = {"status_id": status_id,
                "tag": hashtag}
        if count is not None:
            dic["count"] = count
        return dic

class WordDao(object):
    def __init__(self):
        self.select_query = "SELECT * FROM words WHERE word = %(word)s;"
        self.exists_query = "SELECT 1 FROM words WHERE word = %(word)s;"
        self.insert_query = "INSERT INTO words (word) VALUES (%(word)s);"

    def exists(self, word):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"word": word}
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def insert(self, word, is_ngram=False):
        if self.exists(word):
            return
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = {"word": word, "is_ngram": is_ngram}
                cur.execute(self.insert_query, vals)
            conn.commit()

class WordUseDao(object):
    def __init__(self):
        self.select_query = """SELECT status_id,
                                      word_id,
                                      count
                                 FROM words_use, words
                                WHERE word_id = id
                                  AND word = %(word)s
                                  AND status_id = %(status_id)s;"""
        self.exists_query = """SELECT 1
                                 FROM words_use, words
                                WHERE word_id = id
                                  AND word = %(word)s
                                  AND status_id = %(status_id)s;"""
        self.insert_query = """INSERT INTO words_use (status_id, word_id, count)
                            SELECT %(status_id)s, id, %(count)s FROM words WHERE word = %(word)s;"""

    def exists(self, status, word, use_status_id=False):
        result = None
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(status, word, use_status_id=use_status_id)
                try:
                    cur.execute(self.exists_query, vals)
                except psycopg2.ProgrammingError:
                    result = False
                else:
                    result = cur.fetchone() is not None
        conn.close()
        return result

    def insert(self, status, word, count, use_status_id=False):
        if self.exists(status, word, use_status_id=use_status_id):
            return
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                vals = self._get_dictionary(status, word, count, use_status_id=use_status_id)
                cur.execute(self.insert_query, vals)
            conn.commit()

    def word_list_by_status(self, status_id=None):
        query = """SELECT
                     status_id,
                     impact_on_traffic,
                     w.word,
                     count
                   FROM words_use wu
                     INNER JOIN words w ON (w.id = word_id)
                     INNER JOIN status s ON (s.id = wu.status_id)
                     LEFT JOIN category c ON (s.category_id = c.id)"""
        if status_id is not None:
            query += " WHERE s.id = %d " % status_id
        else:
            query += " ORDER BY status_id; "
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(query)
                    words_use = cur.fetchall()
                except:
                    words_use = []
        conn.close()
        return words_use

    def word_list(self):
        with psycopg2.connect(CONNECTION_STRING) as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("SELECT word FROM words;")
                    word_list = cur.fetchall()
                except:
                    word_list = []
        conn.close()
        return [tp[0] for tp in word_list]

    def _get_dictionary(self, status, word, count=None, use_status_id=False):
        if use_status_id:
            status_id = status
        else:
            status_id = status.id
        dic = {"status_id": status_id,
                "word": word}
        if count is not None:
            dic["count"] = count
        return dic
